class CreatePlans < ActiveRecord::Migration[6.0]
  def change
    create_table :plans do |t|
      t.string :name
      t.integer :status, null: false, default: 0
      t.integer :charge_type, null: false, default: 0
      t.float :default_price
      t.integer :default_trial_days
      t.string :code

      t.timestamps null: false
    end
  end
end


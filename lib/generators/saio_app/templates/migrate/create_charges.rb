class CreateCharges < ActiveRecord::Migration[6.0]
  def change
    create_table :charges do |t|
      t.references :shop, index: true
      t.references :plan, index: true
      t.integer :status, null: false, default: 0
      t.string :name
      t.integer :charge_type, null: false, default: 0
      t.float :price
      t.integer :trial_days
      t.boolean :test, null: false, default: true


      t.timestamps null: false
    end
  end
end


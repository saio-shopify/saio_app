class CreateShops < ActiveRecord::Migration[6.0]

  def change
    unless table_exists? :shops
      create_table :shops  do |t|
        t.string :shopify_domain, null: false
        t.string :shopify_token, null: false
        t.integer :status, null: false, default: 0
        t.jsonb :data, null: false, default: '{}'
        t.string :code

        t.timestamps null: false
      end

      add_index :shops, :shopify_domain, unique: true
    end
  end
end

class ShopUpdateJob < ShopJob

  def perform(shopify_domain, data)
    data ||= ActiveSupport::JSON::decode(@shop.api { ShopifyAPI::Shop.current.to_json })
    @shop.update_attribute(:data, data)
    if @shop.data['plan_name'] == ('cancelled' or 'frozen' or 'fraudulent')
      AppUninstalledJob.perform_later(@shop.shopify_domain)
    #else
      #shop.update
    end
  end
end


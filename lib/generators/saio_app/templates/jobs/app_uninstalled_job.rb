class AppUninstalledJob < ShopJob

  before_enqueue { @shop.awaiting_uninstall! }
  before_perform { @shop.uninstalling! }
  after_perform {
    require 'slack_notify'
    SlackNotify.uninstall(@shop.shopify_domain)
  }

  def perform(domain, shop_data=nil) # shop data is optional
    # Make sure the shop is actually uninstalled before removing
    begin
      # Will succeed if still installed
      @shop.temp { ShopifyAPI::Shop.current }
    rescue
      # If call fails, it's uninstalled, so remove from app
      @shop.uninstall
    end
  end
end


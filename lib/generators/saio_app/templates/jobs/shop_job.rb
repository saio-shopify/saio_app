class ShopJob < ActiveJob::Base
  queue_as :default

  before_perform { |job| set_shop(job) }
  before_enqueue { |job| set_shop(job) }

  private

  def set_shop(job)
    @shop = Shop.find_by!(shopify_domain: job.arguments.first)
    return if @shop.nil?
  end

end


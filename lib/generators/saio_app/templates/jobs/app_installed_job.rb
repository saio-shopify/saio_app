class AppInstalledJob < ShopJob

  before_enqueue { @shop.awaiting_install! }
  before_perform { @shop.installing! }
  after_perform {
    @shop.installed!
    require 'slack_notify'
    SlackNotify.install(@shop.shopify_domain)
  }

  def perform(args)
    ShopUpdateJob.perform_now(@shop.shopify_domain, nil)
  end
end



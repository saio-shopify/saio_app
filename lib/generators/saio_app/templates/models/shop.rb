class Shop < ActiveRecord::Base
  include ShopifyApp::ShopSessionStorage

  # Enums
  enum status: [:never_installed, :awaiting_install, :installing, :installed, :awaiting_uninstall, :uninstalling]

  # Associations
  has_many :charges, dependent: :destroy
  has_one  :active_charge, -> { active }, :class_name=> "Charge"

  # Aliases
  alias_method :temp, :with_shopify_session

  # API Version
  def api_version
    ShopifyApp.configuration.api_version
  end

  # Methods
  def api
    begin
      self.temp { yield }
    rescue ActiveResource::ClientError => api_error
      if api_error.response.code.to_s == "429" # Watch for too many calls
        sleep ENV['API_DELAY'].to_i # Give the api a break, then attempt again
        retry
      else
        raise api_error
      end
    end
  end

  def update
    # Update shop
  end

  def uninstall
    # Shop uninstalled
    # Cancel all charges
    charges.each { |charge| charge.canceled! }

    # Destroy shop info
    destroy
  end
end


class Plan < ActiveRecord::Base

  # Enums
  enum status: [:disabled, :enabled]
  enum charge_type: [:application_charge, :recurring_application_charge]

  # Associations
  has_many :charges

  # Methods

end


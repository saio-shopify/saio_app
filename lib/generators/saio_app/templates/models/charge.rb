class Charge < ActiveRecord::Base

  # Enums
  enum status: [:pending, :accepted, :declined, :active, :canceled, :expired]
  enum charge_type: [:application_charge, :recurring_application_charge]

  # Associations
  belongs_to :shop
  belongs_to :plan

  # Methods
  def self.new_charge(shop_id)
    shop = Shop.find(shop_id)
    plan = Plan.enabled.find_by(code: shop.code) || Plan.enabled.find_by(name: shop.data['plan_name']) || Plan.enabled.find_by(name: "default")
    return nil if plan.default_price <= 0

    # Create a Charge on Shopify
    shopify_charge = shop.temp {
      ShopifyAPI::RecurringApplicationCharge.create(
        name: ENV['APP_NAME'] + "-" + plan.name,
        price: plan.default_price,
        test: ENV['SHOPIFY_TEST_CHARGES'],
        trial_days: plan.default_trial_days,
        return_url: ENV['SHOPIFY_APP_BASE_URL'] + 'charges/activate'
      ) }

    # Create local charge
    Charge.create(
      id: shopify_charge.id,
      shop: shop,
      plan: plan,
      name: plan.name,
      charge_type: plan.charge_type,
      price: plan.default_price,
      trial_days: plan.default_trial_days,
      test: ENV['SHOPIFY_TEST_CHARGES']
    )

    # Return the charge confirmation url
    shopify_charge.confirmation_url
  end

  def self.activate?(shop, charge)
    begin
      shopify_charge = shop.temp { ShopifyAPI::RecurringApplicationCharge.find(charge.id) }
      logger.info(shopify_charge)

      # Set the local charge status to the Shopify charge status
      charge.send("#{shopify_charge.status}!") if charge.respond_to? "#{shopify_charge.status}!"

      logger.info(charge.status)
      return false unless charge.accepted?

      # Activate the Charge on Shopify
      shop.temp { shopify_charge.activate }

      # Cancel the current charge if it's there
      shop.active_charge.canceled! unless shop.active_charge.nil?

      # Set the local charge to be active
      charge.active!
      true
    rescue => e
      logger.info(e)
      false
    end
  end


end


Rails.application.routes.draw do
  mount ShopifyApp::Engine, at: '/'

  # Charge activation route
  get 'charges/activate'

  # GDPR Webhook routes
  post 'redacts/customer_data_request'
  post 'redacts/customer_redact'
  post 'redacts/shop_redact'

  # Add json API routes below
  namespace :v1, defaults: { format: 'json' } do
    resources :shops, only: [:show]
  end
  get '*page', to: 'static#index', constraints: ->(req) do
    !req.xhr? && req.format.html?
  end

  # Forward root to StaticController#index
  root to: 'static#index'
end


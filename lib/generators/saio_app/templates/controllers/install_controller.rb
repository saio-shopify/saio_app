class InstallController < ShopifyApp::AuthenticatedController
  around_action :shopify_session
  skip_before_action :check_active_charge

  def install
    AppInstalledJob.perform_later(@shop.shopify_domain)
    redirect_to action: :installing
  end

  def installing
    if @shop.installed?
      redirect_to root_path
    end
  end
end


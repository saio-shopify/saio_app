class RedactsController < ApplicationController
  include ShopifyApp::WebhookVerification

  def shop_redact
    head :no_content
  end

  def customer_data_request
    head :no_content
  end

  def customer_redact
    head :no_content
  end

end


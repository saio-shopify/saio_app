module ShopifyApp
  class AuthenticatedController < ActionController::Base
    include ShopifyApp::Authenticated
    before_action :set_current_shop
    before_action :check_active_charge
    before_action :check_installed
    around_action :shopify_session
    protect_from_forgery with: :exception


    private

    def set_current_shop
      if shop_session
        @shop ||= Shop.find_by!(shopify_domain: @current_shopify_session.url)
      else
        redirect_to_login and return
      end
    end

    def check_installed
      if @shop.never_installed?
        AppInstalledJob.perform_later(@shop.shopify_domain)
        return
      end
    end

    def check_active_charge
      # Let the test store through
      if @shop.active_charge.nil?
        new_charge_path = Charge.new_charge(@shop.id)
        return if new_charge_path.nil?
        render inline: "<html><body><script type='text/javascript' charset='utf-8'>parent.location.href = '#{new_charge_path}';</script></body></html>"
      end
    end
  end
end


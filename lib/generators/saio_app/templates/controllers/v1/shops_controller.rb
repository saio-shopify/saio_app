class V1::ShopsController < ShopifyApp::AuthenticatedController
  def show
    render json: { shop: @shop }.to_json
  end
end


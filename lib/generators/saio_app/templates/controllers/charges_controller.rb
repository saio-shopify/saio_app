class ChargesController < ShopifyApp::AuthenticatedController
  skip_before_action :check_installed
  skip_before_action :check_active_charge

  def activate
    logger.info("Activating")
    charge = Charge.find_by(id: params[:charge_id])
    if Charge.activate?(@shop, charge)
      flash[:notice] = "App installed successfully!"
      redirect_to root_url
    else
      redirect_to login_path
    end
  end

end


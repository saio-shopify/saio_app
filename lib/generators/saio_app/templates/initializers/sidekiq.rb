Sidekiq.configure_server do |config|
  config.on :startup do
    require 'prometheus_exporter/instrumentation'
    PrometheusExporter::Instrumentation::Process.start type: 'sidekiq'
  end

   config.server_middleware do |chain|
      require 'prometheus_exporter/instrumentation'
      chain.add PrometheusExporter::Instrumentation::Sidekiq
   end

   config.death_handlers << PrometheusExporter::Instrumentation::Sidekiq.death_handler

   at_exit do
     PrometheusExporter::Client.default.stop(wait_timeout_seconds: 10)
   end
end


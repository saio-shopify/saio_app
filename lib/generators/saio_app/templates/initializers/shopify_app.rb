ShopifyApp.configure do |config|
  config.application_name = ENV['APP_NAME']
  config.api_key = ENV['SHOPIFY_CLIENT_API_KEY']
  config.secret = ENV['SHOPIFY_CLIENT_API_SECRET']
  config.scope = ENV['SHOPIFY_SCOPE']
  config.embedded_app = ENV['SHOPIFY_EMBEDDED']
  config.after_authenticate_job = false
  config.api_version = ENV['SHOPIFY_API_VERSION']
  config.shop_session_repository = 'Shop'

  config.webhooks = [
    {topic: 'app/uninstalled', address: ENV['SHOPIFY_APP_BASE_URL'] + 'webhooks/app_uninstalled'},
    {topic: 'shop/update', address: ENV['SHOPIFY_APP_BASE_URL'] + 'webhooks/shop_update'},
  ]
end

require 'rest-client'

class SlackNotify
    @@slack_url = ENV['SLACK_URL']

  def self.install(domain)
    payload = {
      text: "A new shop has installed #{ENV['APP_NAME']}: #{domain}"
    }
    send_message(payload)
  end

  def self.uninstall(domain)
    payload = {
      text: "A shop has uninstalled #{ENV['APP_NAME']}: #{domain}."
      }
    send_message(payload)
  end

  def self.error(domain, error)
    payload = {
      text: "There was a problem with shop: #{domain} at #{Time.now}
      The error was: #{error}"
    }
    send_message(payload)
  end

  def self.send_message(payload)
    resp = RestClient.post(@@slack_url, payload.to_json)
    resp.code
  end

end


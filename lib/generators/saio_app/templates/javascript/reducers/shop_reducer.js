import {
  getShopSuccess
} from '../actions/shop';

const shopReducer = (state = null, action) => {
  switch(action.type) {
    case 'GET_SHOP_SUCCESS':
      return {
        ...state,
        status: action.json.shop.status,
        data: action.json.shop.data,
        updated_at: action.json.shop.updated_at
      }
    default:
      return state;
  }
};

export default shopReducer


import { combineReducers } from 'redux';
import shopReducer from './shop_reducer';
import settingsReducer from './settings_reducer';

export default combineReducers({
  shop: shopReducer,
  settings: settingsReducer,
})


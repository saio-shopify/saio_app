const settingsReducer = (state = null, action) => {
  switch(action.type) {
    case 'TEST_ACTION':
      console.log('Test action reducer!');
      return state;
    default:
      return state;
  }
};

export default settingsReducer


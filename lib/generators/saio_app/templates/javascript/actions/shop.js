export const getShopRequest = (shop_id) => {
  return dispatch => {
    dispatch({
      type: 'GET_SHOP_REQUEST'
    });
    return fetch(`v1/shops/${shop_id}.json`)
      .then(response => response.json())
      .then(json => dispatch(getShopSuccess(json)))
      .catch(error => console.log(error));
  };
};

export const getShopSuccess = (json) => {
  return {
    type: 'GET_SHOP_SUCCESS',
    json: json
  }
}


import React from 'react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import { Page, Layout, EmptyState, Button, Spinner } from '@shopify/polaris';
import loadingImage from '../images/installing.svg';
import { getShopRequest } from '../actions/shop';

class Installing extends React.Component {

  constructor(props) {
    super(props)
    let _this = this;
    this.interval = setInterval(function() {
      _this.props.getShopRequest(_this.props.shop.id)
    },3000);
  }

  stopShopCheck = () => {
    clearInterval(this.interval);
  }

  render() {
    if(this.props.shop.status == "installed") {
      this.stopShopCheck();
      return(
        <Redirect to="/" />
      )
    } else {
      return(
        <Page>
          <Layout>
            <Layout.Section>
              <div className="install-wrapper">
                <EmptyState
                  heading="Please wait!"
                  action={{}}
                  image={loadingImage}
                >
                  <p>We are currently installing the app on your shop.</p>
                  <br />
                  <Spinner />
                </EmptyState>
              </div>
            </Layout.Section>
          </Layout>
        </Page>
      )
    }
  }
}

const mapStateToProps = (state) => ({
  shop: state.shop
})

const mapDispatchToProps = {
  getShopRequest
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Installing);


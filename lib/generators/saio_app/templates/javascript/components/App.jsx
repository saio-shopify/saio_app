import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';
import { Provider as ReduxProvider } from 'react-redux';
import { AppProvider, Page } from '@shopify/polaris';
import { Provider } from '@shopify/app-bridge-react';
import RatingBanner from './RatingBanner';
import Installing from './Installing';
import Index from './Index';
import SaioFooter from './SaioFooter';
import configureStore from '../configureStore';

export default class App extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    const initialState = {
      shop: this.props.shop,
      settings: {
        apiKey: this.props.apiKey,
        host: this.props.host,
        appName: this.props.appName
      }
    }

    const store = configureStore(initialState);
    const config = {
      apiKey: this.props.apiKey,
      shopOrigin: this.props.shop.shopify_domain,
      forceRedirect: true
    }

    return(
      <ReduxProvider store={store}>
        <Provider config={config}>
          <AppProvider>
            <RatingBanner appName={this.props.appName} reviewLink={this.props.reviewLink} supportLink={this.props.supportLink} />
            <BrowserRouter>
              <Switch>
                <Route exact path="/installing" component={Installing} />
                <Route exact path="/" render={() => (
                  store.getState().shop.status == "installing" ?
                  (<Redirect to="/installing" />) :
                  (<Index />)
                )} />
              </Switch>
            </BrowserRouter>
            <SaioFooter apps={this.props.apps} />
          </AppProvider>
        </Provider>
      </ReduxProvider>
    )
  }
}


import React from 'react';
import { connect } from 'react-redux';
import {Page, Button} from '@shopify/polaris';
import { settingsAction } from '../actions/settings';

class Index extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {
    return(
      <Page title="Home">
        <h1>Welcome to your new app({this.props.appName}), {this.props.shop.shopify_domain}</h1>
        <Button onClick={() => this.props.settingsAction()}>Test Reducers</Button>
      </Page>
    )
  }
}

const mapStateToProps = (state) => ({
  shop: state.shop,
  appName: state.settings.appName,
  reviewLink: state.settings.reviewLink,
  supportLink: state.settings.supportLink,
  host: state.settings.host
})

const mapDispatchToProps = {
  settingsAction
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Index);


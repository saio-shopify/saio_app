import React from 'react';
import {CalloutCard, Button} from '@shopify/polaris';

export default class RatingBanner extends React.Component {
  constructor(props) {
    super(props)
  }

  rateApp() {
    window.open(this.props.reviewLink,'_blank');
  }

  getHelp() {
    window.open(this.props.supportLink,'_blank');
  }

  render() {
    let bannerTitle = "Like " + this.props.appName + "?";
    return(
      <div className="banner-wrapper">
        <CalloutCard title={bannerTitle}
          primaryAction={{content: 'Rate App', onAction: () => {this.rateApp()}}}
          secondaryAction={{content: 'Need help? Click here to get support', onAction: () => {this.getHelp()}}} >
          <p>Like our App so far? We will truly appreciate if you could take 2 minutes of your time to leave us a good review.</p>
        </CalloutCard>
      </div>
    )
  }
}


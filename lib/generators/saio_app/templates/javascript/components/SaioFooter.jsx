import React from 'react';
import {Layout, FooterHelp, Link, Heading, Stack} from '@shopify/polaris';
import salesAppImage from '../images/sales-app-image.png'
import termsAppImage from '../images/terms-app-image.png'
import rpcAppImage from '../images/rpc-app-image.png'

export default class SaioFooter extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return(
      <Layout.Section>
        <div className="footer-wrapper">
          <Heading>
            <span className="footer-heading">Other apps from Saio</span>
          </Heading>
          <Stack distribution="center">
            <div key={"footer-app-0"} className="saio-footer-app">
              <Link external url="https://apps.shopify.com/sale-all-in-one">
                <img src={salesAppImage} alt="Sale all in one" style={{width: 300}} />
              </Link>
            </div>

            <div key={"footer-app-1"} className="saio-footer-app">
              <Link external url="https://apps.shopify.com/terms-and-conditions-checkboxes">
                <img src={termsAppImage} alt="Terms and Conditions Checkboxes" style={{width: 300}} />
              </Link>
            </div>

            <div key={"footer-app-2"} className="saio-footer-app">
              <Link external url="https://apps.shopify.com/recent-product-collections">
                <img src={rpcAppImage} alt="Recent Product Collections" style={{width: 300}} />
              </Link>
            </div>
          </Stack>
          <FooterHelp>
            Learn more about <Link external url="https://apps.shopify.com/partners/saio-apps">SAIO Apps</Link>.
          </FooterHelp>
        </div>
      </Layout.Section>
    )
  }
}



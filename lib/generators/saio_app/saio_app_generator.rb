class SaioAppGenerator < Rails::Generators::Base
  include Rails::Generators::Migration

  source_root File.expand_path('../templates', __FILE__)

  def copy_root_files
    %w(.gitignore Procfile Procfile.dev .irbrc .env).each do |file|
      copy_file "root/#{file}", file
    end
  end

  def remove_root_files
    %w(README.rdoc).each do |file|
      remove_file file
    end
  end

  # Configure the application's Gemfile.
  def configure_gems
    # Remove sqlite from the general Gemfile.
    gsub_file 'Gemfile', /^# Use sqlite3 as the database for Active Record\ngem 'sqlite3', '~> 1.4'/m, ''

    # Gems for shopify apps
    gem 'thor'
    gem 'shopify_app'
    gem 'sidekiq', '~> 5.2.7'
    gem 'pg'
    gem 'react-rails'
    gem 'activerecord-import'
    gem 'rest-client'
    gem 'wirble'
    gem 'dotenv'
    gem 'dotenv-rails', require: 'dotenv/rails-now'

    # Monitoring
    #gem 'newrelic_rpm', '~> 3.15.2.317'
    gem 'rollbar'
    gem 'oj'

    # Foreman for development
    gem_group :development do
      gem 'foreman'
    end

    # For development and testing only.
    gem_group :development, :test do
      gem 'minitest-rails'
      gem 'guard'
      gem 'guard-minitest'
    end

    gem_group :test do
      # Webmock for testing external HTTP APIs
      gem 'webmock'
    end
  end

  # Run bundle install to add our new gems before running tasks.
  def bundle_install
    Bundler.with_clean_env do
      run 'bundle install'
      run 'bundle update'
    end
  end

  # Make any required adjustments to the application configuration.
  def configure_application
    # The force_ssl flag is commented by default for production.
    # Uncomment to ensure config.force_ssl = true in production.
    uncomment_lines 'config/environments/production.rb', /force_ssl/

    # Set server side rendereing for components.js
    application "# Enable server side react rendering"

    # Set Sidekiq as the queue adapter in production.
    application "config.active_job.queue_adapter = :sidekiq\n", env: :production
    application "# Use Sidekiq as the active job backend", env: :production

    # Configure React in development and production.
    application "config.react.variant = :development", env: :development
    application "# Use development variant of React in development.", env: :development
    application "config.react.variant = :production", env: :production
    application "# Use production variant of React in production.", env: :production

    # Whitelist ngrok in dev environment
    application(nil, env: "development") do
      "config.hosts << /[a-z0-9]\\D+\\.ngrok\.io/"
    end
  end

  def setup_routes
   #route "root to: 'static#index'"
   #route "# Forward root to StaticController#index"
   #route ""
   #route "end"
   #route "  !req.xhr? && req.format.html?"
   #route "get '*page', to: 'static#index', constraints: ->(req) do"
   #route ""
   #route "end"
   #route "  resource :shop, only: [:show]"
   #route "namespace :v1, defaults: { format: 'json' } do"
   #route "# Add json API routes below"
   #route ""
   #route "post 'redacts/shop_redact'"
   #route "post 'redacts/customer_redact'"
   #route "post 'redacts/customer_data_request'"
   #route "# GDPR Webhook routes"
   #route ""
   #route "get 'charges/activate'"
   #route "# Charge activation route"
  end

  def run_generators
    generate 'react:install'
    generate 'shopify_app:install'
    generate 'shopify_app:shop_model'
  end

  def copy_and_remove_files
    # Copy over the default puma, and sidekiq configurations, plus the dev env variables
    remove_file 'config/database.yml'
    copy_file 'config/database.yml.tt', 'config/database.yml'
    copy_file 'config/puma.rb', 'config/puma.rb'
    copy_file 'config/sidekiq.yml.tt', 'config/sidekiq.yml'
    remove_file 'config/webpacker.yml'
    copy_file 'config/webpacker.yml.tt', 'config/webpacker.yml'
    remove_file "config/routes.rb"
    copy_file "config/routes.rb", "config/routes.rb"

    # Copy initializers
    copy_file 'initializers/shopify_app.rb', 'config/initializers/shopify_app.rb'
    #copy_file 'initializers/sidekiq.rb', 'config/initializers/sidekiq.rb'

    # Copy the shopify controllers
    copy_file 'controllers/static_controller.rb', 'app/controllers/static_controller.rb'
    copy_file 'controllers/install_controller.rb', 'app/controllers/install_controller.rb'
    copy_file 'controllers/charges_controller.rb', 'app/controllers/charges_controller.rb'
    copy_file 'controllers/redacts_controller.rb', 'app/controllers/redacts_controller.rb'
    directory 'controllers/shopify_app', 'app/controllers/shopify_app'
    directory 'controllers/v1', 'app/controllers/v1'
   #copy_file 'controllers/shopify_app/authenticated_controller.rb', 'app/controllers/shopify_app/authenticated_controller.rb'
   #copy_file 'controllers/shopify_app/callback_controller.rb', 'app/controllers/shopify_app/callback_controller.rb'
   #copy_file 'controllers/shopify_app/sessions_controller.rb', 'app/controllers/shopify_app/sessions_controller.rb'
   #copy_file 'controllers/shopify_app/webhooks_controller.rb', 'app/controllers/shopify_app/webhooks_controller.rb'

    # Copy the models
    copy_file 'models/shop.rb', 'app/models/shop.rb'
    copy_file 'models/plan.rb', 'app/models/plan.rb'
    copy_file 'models/charge.rb', 'app/models/charge.rb'

    # Copy assets
    copy_file 'assets/stylesheets/application.scss', 'app/assets/stylesheets/application.scss'
    copy_file 'assets/javascripts/application.js', 'app/assets/javascript/application.js'
    copy_file 'assets/config/manifest.js', 'app/assets/config/manifest.js'

    # Copy the react components
    directory 'javascript/actions/', 'app/javascript/actions'
    directory 'javascript/components/', 'app/javascript/components'
    directory 'javascript/reducers/', 'app/javascript/reducers'
    directory 'javascript/images/', 'app/javascript/images'
    directory 'javascript/packs/', 'app/javascript/packs'
    copy_file 'javascript/configureStore.js', 'app/javascript/configureStore.js'
    remove_dir 'app/javascript/shopify_app'
    remove_file 'app/javascript/packs/hello_react.jsx'

    # Copy the example logo image
    directory 'assets/images/', 'app/assets/images'

    # Copy the slack notify file
    copy_file 'lib/slack_notify.rb', 'lib/slack_notify.rb'
    # Remove application.css
    remove_file 'app/assets/stylesheets/application.css'

    # Remove the layout files created by ShopifyApp
    remove_file 'app/views/layouts/embedded_app.html.erb'
    remove_file 'app/views/layouts/application.html.erb'

    # Copy new layout files
    copy_file 'views/layouts/embedded_app.html.erb', 'app/views/layouts/embedded_app.html.erb'
    copy_file 'views/layouts/application.html.erb', 'app/views/layouts/application.html.erb'

    # Copy the views
    copy_file 'views/static/index.html.erb', 'app/views/static/index.html.erb'
    copy_file 'views/shopify_app/sessions/new.html.erb', 'app/views/shopify_app/sessions/new.html.erb'

    # Copy the jobs
    directory 'jobs/', 'app/jobs'
    remove_file 'app/jobs/application_job.rb'

    # Copy the migration for the models
    version1 = (Time.now.utc - 2.seconds).strftime '%Y%m%d%H%M%S'
    version2 = (Time.now.utc - 1.second).strftime '%Y%m%d%H%M%S'
    version3 = Time.now.utc.strftime '%Y%m%d%H%M%S'

    # Remove the basic shop table migration
    remove_dir "db/migrate"
    #Dir.delete('db/migrate')
    copy_file 'migrate/create_shops.rb', "db/migrate/#{version1}_create_shops.rb"
    copy_file 'migrate/create_plans.rb', "db/migrate/#{version2}_create_plans.rb"
    copy_file 'migrate/create_charges.rb', "db/migrate/#{version3}_create_charges.rb"
  end

  # Create the database seeds
  def create_seeds
    append_to_file 'db/seeds.rb', "Plan.create(name: 'free', status: 'enabled', default_trial_days: 0, default_price: 0, charge_type: 1, code: 'DEVFREE') # for dev shops\n"
    append_to_file 'db/seeds.rb', "Plan.create(name: 'default', status: 'enabled', default_trial_days: 5, default_price: 3.99, charge_type: 1)"
  end

  # Create PG database
  def create_database
    rake 'db:create'
  end

  # Run migrations.
  def migrate
    rake 'db:migrate'
  end

  # Seed the database
  def seed
    rake 'db:seed'
  end

  # Lock down the application to a specific Ruby version:
  #
  #  - Via .ruby-version file for rbenv in development;
  #  - Via a Gemfile line in production.
  #
  # This should be the last operation, to allow all other operations to run in the initial Ruby version.
  def set_ruby_version
    prepend_to_file 'Gemfile', "ruby '2.7.2'\n"
  end
end


$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "saio_app/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "saio_app"
  s.version     = SaioApp::VERSION
  s.authors     = ["Eric Gowens"]
  s.email       = ["mail@ericgowens.com"]
  s.homepage    = "https://bitbucket.org/egowens/saio_app"
  s.summary     = "Rails engine for generating common SAIO Shopify App code"
  s.description = "Rails engine for generating common SAIO Shopify App code"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 6.1.0"
  s.add_dependency 'shopify_app'
  s.add_dependency 'puma'
  s.add_dependency 'sidekiq', '~> 5.2.7'
  s.add_dependency 'pg', '~> 1.2.3'
  s.add_dependency 'react-rails', '~> 2.6.1'
  s.add_dependency 'dotenv', '~> 2.7.6'

  s.add_development_dependency 'minitest-reporters'
  s.add_development_dependency 'guard'
  s.add_development_dependency 'guard-minitest'
  s.add_development_dependency 'webmock'

end

